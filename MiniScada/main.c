//
//  main.c
//  MiniScada
//
//  Created by Julian A Guarin on 4/8/15.
//  Copyright (c) 2015 fitech giga. All rights reserved.
//
#include <assert.h>
#include <stdio.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>
#include <stdio.h>

#define STATUS_BYTE 0x0F

void desplegarResultadoRequest(unsigned char * trama_de_recepcion){
    
    unsigned short * apuntador_a_dos_bytes;
    if (trama_de_recepcion[0] == 0xFF) {
        printf("El Robot no está disponible......\n");
        return;
    } else if (trama_de_recepcion[0] != 0xAA) {
        printf("Error Fatal! Llame a mesa de ayuda y coloque el incidente\n");
        return;
    }
    /* Desplegar la información enviada */
    apuntador_a_dos_bytes = (unsigned short*) &trama_de_recepcion[1];
    printf("Ángulo del manipulador: %hu\n", apuntador_a_dos_bytes[0]);
    printf("Coord Z del manipulador: %u\n",trama_de_recepcion[3]);
    printf("Coord R del manipulador: %u\n",trama_de_recepcion[4]);
    if (trama_de_recepcion[5] == 1) {
        printf("El Manipulador Roboótico se está moviendo\n");
    } else printf("El Manipulador Robótico está quieto\n");

}

void statusRequest(char*url){
    
    int conexion,bytes,resultado_shutdown;
    unsigned char * trama_de_recepcion = NULL;
    
    
    /* 1. CUADRAR TRAMA */
    unsigned char trama = STATUS_BYTE;
    
    /* 2. ESTABLECER LA CONEXION */
    int sock = nn_socket (AF_SP, NN_REQ);
    assert (sock >= 0);
    
    conexion = nn_connect (sock, url);
    assert(conexion >= 0);
    
    //int sz_date = strlen(DATE) + 1; // '\0' too
    //char *buf = NULL;
    //int bytes = -1;
    printf ("Enviando Status Request...... \n");
    
    /* 2b. Enviar */
    bytes = nn_send(sock, &trama, 1, NN_DONTWAIT);
    assert (bytes == 1);
    
    /* 2c. Recibir */
    bytes = nn_recv (sock, &trama_de_recepcion, NN_MSG, 0);
    assert (bytes >= 0);
    
    /* 2d. Desconectar */
    resultado_shutdown = nn_shutdown(sock, 0);
    
    /* 3. Desplegar el resultado */
    desplegarResultadoRequest(trama_de_recepcion);
    nn_freemsg (trama_de_recepcion);
    
}

int main(int argc, const char * argv[]) {
    
    char opcion;
    do{
        /* DESPLEGAR EL MENU */
        printf("MENU\n");
        printf("1. STATUS\n2. COMANDO\n3. SALIR\n");
        
        
        /* PREGUNTAR POR UNA OPCION */
        printf("Escoja una opción:");
        scanf("%c",&opcion);
        
        
        /* EJECUTAR EL COMANDO */
        
        if (opcion=='1')
            printf("ENVIAR REQUEST DE STATUS!\n");
        else if (opcion=='2')
            printf("ENVIAR UN COMANDO CON ANGULO,Z,RADIO!\n");
        else if (opcion=='3')
            return 0;
        else
            printf("\nERROR: OPCION INVALIDA\n");
        
    }while (1);
    

    
    
    
    /* SALIR */




}
