#include <assert.h>
#include <stdio.h>
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>

#define STATUS_REQUEST  0x0f
#define COMMAND_REQUEST 0x0e


#define STATUS_IS_OK    0xAA
#define STATUS_IS_FCKUP 0xFF

typedef struct robot{
    
    
    unsigned short angulo;
    unsigned char z;
    unsigned char r;
    unsigned char isMoving;
    
    
}robotState;


robotState robot;
void displayRobotPosition(robotState * probot){
    
    printf("Angulo:\t%hu\n",probot->angulo);
    printf("Z:\t\t%hhu\n",  probot->z);
    printf("r:\t\t%hhu\n",  probot->r);
    
}
int Servidor(const char *url)
{
    unsigned char reply_trama[6];
    
    //Conectarse
    int sock = nn_socket (AF_SP, NN_REP);
    assert (sock >= 0);
    
    //Escuchar en la URL
    assert (nn_bind (sock, url) >= 0);
    
    while (1)
    {
        char *buf = NULL;
        int bytes = nn_recv (sock, &buf, NN_MSG, 0);
        assert (bytes >= 0);
        
        switch (buf[0]) {
            case STATUS_REQUEST:
                
                reply_trama[0] = STATUS_IS_OK;
                
                unsigned short * pAngulo = (unsigned short*)(&reply_trama[1]);
                pAngulo[0] = robot.angulo;
                reply_trama[3] = robot.z;
                reply_trama[4] = robot.r;
                reply_trama[5] = robot.isMoving;
                nn_send(sock, &reply_trama, 6, 0);
                
                break;
            
            case COMMAND_REQUEST:
                
                robot.angulo = buf[2];
                robot.angulo = robot.angulo<<8;
                robot.angulo = robot.angulo+buf[1];
                
                robot.z = buf[3];
                robot.r = buf[4];
                
                printf("Robot is moving to: ");
                displayRobotPosition(&robot);
                
                reply_trama[0] = STATUS_IS_OK;
                
                nn_send(sock, &reply_trama, 1, 0);
                
            default:
                break;
        }
        
        nn_freemsg (buf);
    }
    return nn_shutdown (sock, 0);
}


int main (const int argc, const char **argv)
{
    
    robot.angulo=0;
    robot.r=100;
    robot.z=50;
    robot.isMoving=0;
    
    const char * url = argv[1];
    Servidor(url);
    return 0;
}